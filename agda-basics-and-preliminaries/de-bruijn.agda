{-# OPTIONS --safe --without-K #-}

-- file loaded with Agda version 2.6.0

open import Data.Nat using (ℕ; suc)
open import Data.Fin using (Fin; zero; suc)
open import Data.Vec using (Vec; _∷_; lookup)

data Type : Set where
  base : Type
  arrow : Type → Type → Type

data Term (n : ℕ) : Set where
  ind : Fin n → Term n
  abs : Type → Term (suc n) → Term n
  app : Term n → Term n → Term n
  
_ : Term 0
_ = abs (arrow base base) (abs base (app (ind (suc zero)) (ind zero)))

Context = Vec Type

data Typing {n : ℕ} (Γ : Context n) :
  Term n → Type → Set where
  typing-ind :
    (i : Fin n) →
    Typing Γ (ind i) (lookup i Γ)
  typing-abs : ∀ {t t′ e} →
    Typing (t ∷ Γ) e t′ →
    Typing Γ (abs t e) (arrow t t′)
  typing-app : ∀ {t t′ e e′} →
    Typing Γ e (arrow t t′) →
    Typing Γ e′ t →
    Typing Γ (app e e′) t′
