{-# OPTIONS --safe --without-K #-}

-- file loaded with Agda version 2.6.0

open import Relation.Binary.PropositionalEquality using (refl)
open import Data.List using (List; []; _∷_)
open import Data.List.Membership.Propositional using (_∈_)
open import Data.List.Any using (here; there)

data Type : Set where
  base : Type
  arrow : Type → Type → Type

Context = List Type

data Term (Γ :  Context) : Type → Set where
  ind : ∀ {t} → t ∈ Γ → Term Γ t
  abs : ∀ t {t′} → Term (t ∷ Γ) t′ → Term Γ (arrow t t′)
  app : ∀ {t t′} → Term Γ (arrow t t′) → Term Γ t → Term Γ t′

_ : Term [] (arrow (arrow base base) (arrow base base))
_ = abs (arrow base base) (abs base (app (ind (there (here refl))) (ind (here refl))))
