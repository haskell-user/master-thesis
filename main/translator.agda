{-# OPTIONS --safe --without-K #-}

-- file loaded with Agda version 2.6.0

open import Data.Unit using (tt)
open import Data.Product using (_,_)
open import Data.List using (List; []; _∷_)
open import Data.List.Any using (here; there)
open import Data.List.Membership.Propositional using (_∈_)
open import Relation.Binary.PropositionalEquality using (_≡_; refl)

open import defs
open import while
open import flowgraph

translateExpr : {Γ : Context} {t : Type} (expr : Expr Γ t)
  {labels : List StackTypes} {stack-types : StackTypes} →
  FlowGraph labels Γ (t ∷ stack-types) →
  FlowGraph labels Γ stack-types

translateExpr (var x) fg = jf-instr (load x) fg

translateExpr (lit val) fg = jf-instr (push val) fg

translateExpr (add e e′) fg =
  translateExpr e (translateExpr e′ (jf-instr add fg))

translateExpr (mul e e′) fg =
  translateExpr e (translateExpr e′ (jf-instr mul fg))

translateExpr (eq e e′) fg =
  translateExpr e (translateExpr e′ (jf-instr eq fg))

translateExpr (le e e′) fg =
  translateExpr e (translateExpr e′ (jf-instr le fg))

translateStmt : {Γ : Context} → Stmt Γ → {labels : List StackTypes} →
  FlowGraph labels Γ [] → FlowGraph labels Γ []

translateStmts : {Γ : Context} → List (Stmt Γ) → {labels : List StackTypes} →
  FlowGraph labels Γ [] → FlowGraph labels Γ []

translateStmt (assign x expr) fg =
  translateExpr expr (jf-instr (store x) fg)
  
translateStmt (if cond then else) fg =
  fglet fg
    (fglet (translateStmts then (goto (here refl)))
      (fglet (translateStmts else (goto (there (here refl))))
        (translateExpr cond (branch (there (here refl)) (here refl)))))

translateStmt (while cond body) fg =
  fglet fg
    (fgfix
      (fglet (translateStmts body (goto (here refl)))
        (translateExpr cond (branch (here refl) (there (there (here refl)))))))

translateStmts [] fg = fg

translateStmts (stmt ∷ stmts) fg =
  translateStmt stmt (translateStmts stmts fg)

translate : {Γ : Context} → List (Stmt Γ) → FlowGraph [] Γ []

translate prog = translateStmts prog stop

translateState : {Γ : Context} → State Γ → Env Γ

translateState {[]} tt = tt
translateState {_ ∷ _} (val , σ) = (val , translateState σ)

lookup-var-to-env-load : {Γ : Context} {t : Type} (t-in-Γ : t ∈ Γ) (σ : State Γ) →
  lookup-var t-in-Γ σ ≡ env-load t-in-Γ (translateState σ)
lookup-var-to-env-load (here refl) _ = refl
lookup-var-to-env-load (there t-in-Γ) (_ , σ)
  rewrite lookup-var-to-env-load t-in-Γ σ = refl

update-state-to-env-store : {Γ : Context} {t : Type} (type-in-Γ : t ∈ Γ) (σ : State Γ) (val : Val t) →
  translateState (update-state type-in-Γ σ val) ≡ env-store type-in-Γ (translateState σ) val
update-state-to-env-store (here refl) σ _ = refl
update-state-to-env-store (there type-in-Γ) (_ , σ) val
  rewrite update-state-to-env-store type-in-Γ σ val = refl

translateExpr-correct :
  ∀ {Γ t expr σ labels stack-types stack-types′ fg env}
  {fgs : flowgraphs-for-labels Γ labels}
  {st : Stack stack-types} {st′ : Stack stack-types′}
  {val : Val t} → EvalExpr σ expr val →
  EvalFlowGraph fgs (translateState σ) (val , st) fg env st′ →
  EvalFlowGraph fgs (translateState σ) st (translateExpr expr fg) env st′

translateExpr-correct {σ = σ} (eval-var x) eval-fg
  rewrite lookup-var-to-env-load x σ =
  eval-jf-instr (eval-load _ x) eval-fg

translateExpr-correct (eval-lit val) eval-fg =
  eval-jf-instr (eval-push _ val) eval-fg

translateExpr-correct (eval-add eval-e eval-e′) eval-fg =
  translateExpr-correct eval-e
    (translateExpr-correct eval-e′
      (eval-jf-instr (eval-add _ _ _) eval-fg))

translateExpr-correct (eval-mul eval-e eval-e′) eval-fg =
  translateExpr-correct eval-e
    (translateExpr-correct eval-e′
      (eval-jf-instr (eval-mul _ _ _) eval-fg))

translateExpr-correct (eval-eq eval-e eval-e′) eval-fg =
  translateExpr-correct eval-e
    (translateExpr-correct eval-e′
      (eval-jf-instr (eval-eq _ _ _) eval-fg))

translateExpr-correct (eval-le eval-e eval-e′) eval-fg =
  translateExpr-correct eval-e
    (translateExpr-correct eval-e′
      (eval-jf-instr (eval-le _ _ _) eval-fg))

translateStmt-correct :
  ∀ {Γ stmt σ σ′ env labels fg}
  {fgs : flowgraphs-for-labels Γ labels} →
  EvalStmt σ stmt σ′ →
  EvalFlowGraph fgs (translateState σ′) tt fg env tt →
  EvalFlowGraph fgs (translateState σ) tt (translateStmt stmt fg) env tt

translateStmts-correct :
  ∀ {Γ stmts σ σ′ env labels fg}
  {fgs : flowgraphs-for-labels Γ labels} →
  EvalStmts σ stmts σ′ →
  EvalFlowGraph fgs (translateState σ′) tt fg env tt →
  EvalFlowGraph fgs (translateState σ) tt (translateStmts stmts fg) env tt

translateStmt-correct {σ = σ} (eval-assign {val = val} eval-expr x) eval-fg
  rewrite update-state-to-env-store x σ val =
  translateExpr-correct eval-expr (eval-jf-instr (eval-store _ x val) eval-fg)

translateStmt-correct (eval-if-false eval-cond-false eval-else _) eval-fg =
  eval-let (eval-let (eval-let
    (translateExpr-correct eval-cond-false (eval-branch-false
      (translateStmts-correct eval-else (eval-goto eval-fg))))))

translateStmt-correct (eval-if-true eval-cond-true eval-then _) eval-fg =
  eval-let (eval-let (eval-let
    (translateExpr-correct eval-cond-true (eval-branch-true
      (translateStmts-correct eval-then (eval-goto eval-fg))))))

translateStmt-correct (eval-while-false eval-cond-false _) eval-fg =
  eval-let (eval-fix (eval-let
    (translateExpr-correct eval-cond-false (eval-branch-false eval-fg))))

translateStmt-correct (eval-while-true eval-cond-true eval-body eval-stmt) eval-fg
  with translateStmt-correct eval-stmt eval-fg
... | eval-let eval-let-body =
  eval-let (eval-fix (eval-let
    (translateExpr-correct eval-cond-true (eval-branch-true
      (translateStmts-correct eval-body (eval-goto eval-let-body))))))

translateStmts-correct eval-skip eval-fg = eval-fg

translateStmts-correct (eval-seq eval-stmt eval-stmts) eval-fg =
  translateStmt-correct eval-stmt (translateStmts-correct eval-stmts eval-fg)

translate-correct : ∀ {Γ} {prog : List (Stmt Γ)} {σ σ′} →
  EvalStmts σ prog σ′ →
  EvalFlowGraph tt (translateState σ) tt (translate prog) (translateState σ′) tt

translate-correct eval-prog = translateStmts-correct eval-prog (eval-stop _)
