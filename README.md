This repository contains the source files of my master thesis "Verified Compilation to Intrinsically Typed Control-Flow Graphs in Agda".

It describes a certain control-flow graph representation whose syntax is given by an intrinsically typed syntax (intrinsic typed syntax is explained [here](http://casperbp.net/papers/intrinsicallytyped.html)) and whose semantics is given by a big-step operational semantics.
Furthermore, it describes a verified translation of while programs to these control-flow graphs.

The development is carried out in version 2.6.0 of [Agda](https://agda.readthedocs.io/).

## Folders ##

* *agda-basics-and-preliminaries* : Agda definitions of agda and preliminaries chapters
* *main* : Agda definitions of the main part of the thesis

### Files in main ###

* *defs.agda* : definition of types and values of the while language and flow graphs
* *while.agda* : intrinsically typed syntax and big-step operational semantics of a while language
* *flowgraph.agda* : intrinsically typed syntax and big-step operational semantics of the flow graph representation
* *translator.agda* : translator implementation and correctness proof
* *flowgraph-examples.agda* : some flow graph examples
* *factorial.agda* : example translation of a while program that computes the factorial function
