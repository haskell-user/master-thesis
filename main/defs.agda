{-# OPTIONS --safe --without-K #-}

-- file loaded with Agda version 2.6.0

open import Data.Bool using (Bool)
open import Data.Integer using (ℤ)

data Type : Set where
  num bool : Type

Val : Type → Set
Val num = ℤ
Val bool = Bool
