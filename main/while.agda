{-# OPTIONS --safe --without-K #-}

-- file loaded with Agda version 2.6.0

open import Data.Unit using (⊤)
open import Data.Product using (_×_; _,_)
open import Data.Bool using (false; true)
open import Data.Integer using (ℤ; _+_; _*_; _≟_; _≤?_; +_)
open import Data.List using (List; []; _∷_)
open import Data.List.Membership.Propositional using (_∈_)
open import Data.List.Any using (here; there)
open import Relation.Binary.PropositionalEquality using (refl)
open import Relation.Nullary.Decidable using (⌊_⌋)

open import defs

-- syntax

Context = List Type

data Expr (Γ : Context) : Type → Set where
  var :  {t : Type} → t ∈ Γ → Expr Γ t
  lit :  {t : Type} → Val t → Expr Γ t
  add : (e e′ : Expr Γ num) → Expr Γ num
  mul : (e e′ : Expr Γ num) → Expr Γ num
  eq  : (e e′ : Expr Γ num) → Expr Γ bool
  le  : (e e′ : Expr Γ num) → Expr Γ bool

data Stmt (Γ : Context) : Set where
  assign : {t : Type} → t ∈ Γ → Expr Γ t → Stmt Γ
  if     : (cond : Expr Γ bool) (then else : List (Stmt Γ)) → Stmt Γ
  while  : (cond : Expr Γ bool) (body : List (Stmt Γ)) → Stmt Γ

_ : (Γ : Context) → Expr Γ num
_ = λ Γ → add (lit (+ 1)) (lit (+ 2))

_ : Expr (num ∷ num ∷ []) bool
_ = le (var (here refl)) (var (there (here refl)))

-- semantics

State : Context → Set
State [] = ⊤
State (t ∷ Γ) = Val t × State Γ

lookup-var : {Γ : Context} {t : Type} → t ∈ Γ → State Γ → Val t
lookup-var (here refl) (val , _) = val
lookup-var (there t-therein) (_ , σ) = lookup-var t-therein σ

update-state : {Γ : Context} {t : Type} → t ∈ Γ → State Γ → Val t → State Γ
update-state (here refl) (_ , σ) new-val = new-val , σ
update-state (there t-therein) (val , σ) new-val = val , update-state t-therein σ new-val

data EvalExpr {Γ : Context} (σ : State Γ) :
  {t : Type} → Expr Γ t → Val t → Set where
  eval-var : ∀ {t} (x : t ∈ Γ) →
    EvalExpr σ (var x) (lookup-var x σ)
  eval-lit : ∀ {t} (val : Val t) →
    EvalExpr σ (lit val) val
  eval-add : ∀ {e e′ i j} →
    EvalExpr σ e i →
    EvalExpr σ e′ j →
    EvalExpr σ (add e e′) (i + j)
  eval-mul : ∀ {e e′ i j} →
    EvalExpr σ e i →
    EvalExpr σ e′ j →
    EvalExpr σ (mul e e′) (i * j)
  eval-eq :  ∀ {e e′ i j} →
    EvalExpr σ e i →
    EvalExpr σ e′ j →
    EvalExpr σ (eq e e′) (⌊ i ≟ j ⌋)
  eval-le :  ∀ {e e′ i j} →
    EvalExpr σ e i →
    EvalExpr σ e′ j →
    EvalExpr σ (le e e′) (⌊ i ≤? j ⌋)

mutual
  data EvalStmt {Γ : Context} (σ : State Γ) :
    Stmt Γ → State Γ → Set where
    eval-assign : ∀ {t expr val} →
      EvalExpr σ expr val → (x : t ∈ Γ) →
      EvalStmt σ (assign x expr) (update-state x σ val)
    eval-if-false : ∀ {cond else σ′} →
      EvalExpr σ cond false →
      EvalStmts σ else σ′ →
      (then : List (Stmt Γ)) →
      EvalStmt σ (if cond then else) σ′
    eval-if-true : ∀ {cond then σ′} →
      EvalExpr σ cond true →
      EvalStmts σ then σ′ →
      (else : List (Stmt Γ)) →
      EvalStmt σ (if cond then else) σ′
    eval-while-false : ∀ {cond} →
      EvalExpr σ cond false →
      (body : List (Stmt Γ)) →
      EvalStmt σ (while cond body) σ
    eval-while-true : ∀ {cond body σ′ σ″} →
      EvalExpr σ cond true →
      EvalStmts σ body σ′ →
      EvalStmt σ′ (while cond body) σ″ →
      EvalStmt σ (while cond body) σ″
  data EvalStmts {Γ : Context} (σ : State Γ) :
    List (Stmt Γ) → State Γ → Set where
    eval-skip :
      EvalStmts σ [] σ
    eval-seq : ∀ {stmt stmts σ′ σ″} →
      EvalStmt σ stmt σ′ →
      EvalStmts σ′ stmts σ″ →
      EvalStmts σ (stmt ∷ stmts) σ″
