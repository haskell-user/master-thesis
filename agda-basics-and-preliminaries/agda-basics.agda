{-# OPTIONS --safe --without-K #-}

-- file loaded with Agda version 2.6.0

data Nat : Set where
  zero : Nat
  suc  : Nat → Nat
{-# BUILTIN NATURAL Nat #-}

data Bool : Set where
  false true : Bool

data List {a} (A : Set a) : Set a where
  []  : List A
  _∷_ : (x : A) (xs : List A) → List A
infixr 5 _∷_

l1 : List Nat
l1 = 0 ∷ 1 ∷ 2 ∷ []

l2 : List Nat
l2 = 0 ∷ 1 ∷ 1 ∷ 0 ∷ []

l3 : List Set
l3 = Bool ∷ Nat ∷ []

data Vec {a} (A : Set a) : Nat → Set a where
  []  : Vec A zero
  _∷_ : ∀ {n} (x : A) (xs : Vec A n) → Vec A (suc n)

v1 : Vec Nat 3
v1 = 0 ∷ 1 ∷ 2 ∷ []

_+_ : Nat → Nat → Nat
zero  + m = m
suc n + m = suc (n + m)

mutual
  data Even : Nat → Set where
    even-zero : Even zero
    even-suc : {n : Nat} → Odd n → Even (suc n)
  data Odd : Nat → Set where
    odd-suc : {n : Nat} → Even n → Odd (suc n)

even-0 : Even 0
even-0 = even-zero

odd-1 : Odd 1
odd-1 = odd-suc even-zero

even-2 : Even 2
even-2 = even-suc (odd-suc even-zero)

even+even : {n m : Nat} → Even n → Even m → Even (n + m)

odd+even : {n m : Nat} → Odd n → Even m → Odd (n + m)

even+even even-zero even-m = even-m
even+even (even-suc odd-n) even-m = even-suc (odd+even odd-n even-m)

odd+even (odd-suc even-n) even-m = odd-suc (even+even even-n even-m)
